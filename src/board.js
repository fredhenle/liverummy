import React from 'react';
import { Helmet } from 'react-helmet';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import pop from './pop.mp3';
import psst from './psst.mp3';
import whoosh from './whoosh.mp3';
import whistle from './whistle.mp3';

var classNames = require('classnames');

const isWild = (i, wild2) => ( i < 0 || (wild2 && i % 13 === 1) );

const SortableItem = SortableElement(({ value, next, myClick, myG, plaincard, sortIndex }) => <li><Card sortIndex={sortIndex} value={value} next={next} myClick={myClick} myG={myG} plaincard={plaincard} /></li>);
const SortableList = SortableContainer(({ items, next, myClick, myG, plaincard }) => (
    <ul>
        {items.map((value, index) => (
            <SortableItem key={`item-${value}`} index={index} sortIndex={(index+1)/(items.length+1)} value={value} next={next} myClick={myClick} myG={myG} plaincard={plaincard} />
        ))}
    </ul>
));
const SortableComponent = ({ items, next, myClick, myG, mySortStart, mySortEnd, plaincard }) => (
    <SortableList axis='xy' distance={5} items={items} next={next} myClick={myClick} myG={myG} onSortStart={mySortStart} onSortEnd={mySortEnd} plaincard={plaincard} helperClass='drag' />
);

const Button = ({ myClick, text, id, myMouseEnter, myMouseOut }) => {
    let up = id === 'disc' || id === 'sell';
    return <button
        className={classNames({'rummy': true, 'up': up})}
        onClick={() => myClick(id)}
        onMouseEnter={() => myMouseEnter(id)}
        onMouseOut={() => myMouseOut(id)}
    >
        {text}
    </button>
}

const cardBases = [127185, 127169, 127137, 127153]; // Unicode for aces of clubs, diamonds, spades, hearts
const cardRank = (value) => ( value % 13 + Math.floor(value % 13 / 11) );
const cardSuit = (value) => ( Math.floor(value % 52 / 13) );
const cardString = (value) => ( String.fromCodePoint(
    value == null ? 127136 : value < 0 ? 127199 : cardBases[cardSuit(value)] + cardRank(value)
));
const Rank = (i) => ( i < 0 ? -1 : i % 13 );
const Suit = (i) => ( i < 0 ? -1 : Math.floor(i % 52 / 13) );
const plainCardString = (i) => (
//    i == null ? String.fromCodePoint(10086) : i < 0 ? '%' + String.fromCodePoint(9734) : ['A','2','3','4','5','6','7','8','9',String.fromCodePoint(9361),'J','Q','K'][Rank(i)] + String.fromCodePoint([9831, 9826, 9828, 9825][Suit(i)])
    i == null ? String.fromCodePoint(10086) : i < 0 ? '%' + String.fromCodePoint(9733) : ['A','2','3','4','5','6','7','8','9',String.fromCodePoint(9361),'J','Q','K'][Rank(i)] + String.fromCodePoint([9827, 9830, 9824, 9829][Suit(i)])
);
const Card = ({ value, next, lit, myClick, myMouseEnter, myMouseOut, myG, plaincard, sortIndex }) => {
    let color = value == null ? 'facedown' : isWild(value, myG.wild2) ? 'wild' : (cardSuit(value) % 2 ? 'red' : 'black') + String(1+Math.floor(cardSuit(value) / 2));
    return <span style={plaincard ? {} : {'transform':`rotate(${-30+60*sortIndex}deg)`,'bottom':`${-280+300*Math.cos(3.14159/6*(1.2-2*sortIndex))}px`}}
        className={classNames({
            narrowcard: typeof lit === 'undefined' && plaincard === 0,
            widecard: typeof lit !== 'undefined' || plaincard === 1,
            wideplaincard: plaincard >= 2,
        })}><span
        className={classNames({
            outercard: plaincard < 2,
            outerplaincard: plaincard >= 2,
            lit: lit || (value != null && next === value),
        })}
        onClick={myClick && (() => myClick(value))}
        onMouseEnter={myMouseEnter && (() => myMouseEnter(value))}
        onMouseOut={myMouseOut && (() => myMouseOut(value))}
        ><span
        className={classNames({
            innercard: plaincard < 2,
            innerplaincard: plaincard >= 2,
            innerplaincard1: plaincard === 2,
            innerplaincard2: plaincard === 3,
            facedown: color === 'facedown',
            wild: color === 'wild',
            diamond: color === 'red1',
            club: color === 'black1',
            heart: color === 'red2',
            spade: color === 'black2',
        })}
    >
    {(plaincard >= 2) ? plainCardString(value) : cardString(value)}
    </span></span></span>;
};

const Hand = ({ hand, value, myG, next, myClick, mySortStart, mySortEnd, myMouseEnter, myMouseOut, plaincard }) => (
    hand
        ? <SortableComponent items={value} myG={myG} next={next} myClick={myClick} mySortStart={mySortStart} mySortEnd={mySortEnd} plaincard={plaincard} />
        : <span>{value.map(i => <Card value={i} next={next} myClick={myClick} myMouseEnter={myMouseEnter} myMouseOut={myMouseOut} myG={myG} plaincard={plaincard} />)}</span>
);

const Pile = ({ myClick, myMouseEnter, myMouseOut, myG, value, show, want, plaincard }) => (
    <span><span className='vertical'>{value.length} card{value.length !== 1 ? 's' : ''}</span>{
        value.length > 0 ? <Card value={show ? value[value.length-1] : null} lit={want} myClick={myClick} myMouseEnter={myMouseEnter} myMouseOut={myMouseOut} myG={myG} plaincard={plaincard} /> : ''
    }</span>
);

const GoalDesc = (code) => {
    let sets = code.split('s').length - 1;
    let runs = code.split('r').length - 1;
    sets = `${sets} set${sets !== 1 ? 's' : ''}`;
    runs = `${runs} run${runs !== 1 ? 's' : ''}`;
    return `${sets}, ${runs}`;
};

const GoalDisabled = (G, ctx, p, r) => (G.alternate ? (G.round === 6 ? r !== 6 : r === 6 || new Set(G.melds[p]).has(r)) : r !== G.round)

const GoalOptions = (G, ctx, p) => (G.goals.map((code, index) => (<option key={index + 1} value={index + 1} disabled={GoalDisabled(G, ctx, p, index + 1)}>{GoalDesc(code)}</option>)));

export class rummyboard extends React.Component {
    state = {
        help: false,
        debug: false,
        donotdiscard: false,
        drop: null,
        plaincard: 0,
        pop: 0,
        psst: false,
        whoosh: 0,
        whistle: 0,
    };

    playerName = (p) => {
        let G = this.props.G;
        if (G && G.names && G.names[p]) {
            return G.names[p];
        }
        let n = G.numPlayers;
        let m = this.props.matchData;
        let lobby_name = (m && n > 1) ? m.find(e => e.id === p).name : null;
        if (lobby_name == null) {
            lobby_name = `Player ${p}`;
        }
        this.props.moves.Rename(p, lobby_name);
        return lobby_name;
    }

    playAudio = (id) => {
        let a = document.getElementsByClassName('audio-element');
        if (a && a[id]) {
            a[id].play()
        }
    }

    onMouseEnter = (id) => {
        if (this.state.drop !== null && id !== null) {
            this.setState({drop:id});
        }
    }

    onMouseOut = (id) => {
        if (this.state.drop) {
            this.setState({drop:''});
        }
    }

    onClick = (id) => {
        let G = this.props.G;
        let ctx = this.props.ctx;
        let this_player = parseInt(this.props.playerID, 10);
        let current_player = parseInt(ctx.currentPlayer, 10);
        let stage = this.props.ctx.activePlayers[current_player];
        if (id === 'freq' || id === 'rank' || id === 'suit' || id === 'cost') {
            this.props.moves.Sort(this_player, id);
        } else if (id === 'auto') {
            this.props.moves.AutoMeld(this_player);
        } else if (id === 'disp') {
            this.setState({plaincard:(this.state.plaincard + 1) % 4});
        } else if (G.players[this_player].hand.includes(id)) {
            this.setState({donotdiscard:false});
            this.props.moves.Ready(this_player, id);
        } else if (this_player === current_player) {
            if (id === 'deal') {
                this.props.moves.Deal();
            } else if (id === 'meld') {
                if (stage === 'play') {
                    this.props.moves.Meld(this_player);
                }
            } else if (id === 'disc') {
                if (stage === 'play' || stage === 'discard') {
                    if (! this.state.donotdiscard) {
                        this.props.moves.Discard(this_player);
                    }
                }
            } else if (id === 'sell' || id == null) {
                this.setState({donotdiscard:false});
                this.props.moves.Draw(true);
            } else if (id === G.discards[G.discards.length - 1]) {
                if (stage === 'play' || stage === 'discard') {
                    if (! this.state.donotdiscard) {
                        this.props.moves.Discard(this_player);
                    }
                } else {
                    this.setState({donotdiscard:true});
                    this.props.moves.Draw(false);
                }
            } else {
                this.props.moves.Toss(this_player, id);
            }
        } else if (id === G.discards[G.discards.length - 1]) {
            if (stage === 'draw') {
                this.props.moves.Buy(this_player);
            } else if (stage === 'play') {
                this.props.moves.Steal(this_player, id);
            }
        }
    }

    onSortStart = ({ node, index }) => {
        this.setState({donotdiscard:false});
        this.props.moves.ReadyIndex(parseInt(this.props.playerID, 10), index);
        this.setState({drop:''});
    }

    onSortEnd = ({ oldIndex, newIndex }) => {
        let p = parseInt(this.props.playerID, 10);
        if (this.state.drop) {
            let discards = this.props.G.discards;
            let discard = (discards.length > 0) ? discards[discards.length - 1] : 'disc';
            if (this.state.drop === discard) {
                this.props.moves.Discard(p);
            } else if (isNaN(this.state.drop)) {
                this.props.moves.Reorder(p, oldIndex, newIndex);
            } else {
                this.props.moves.Toss(p, this.state.drop);
            }
        } else {
            this.props.moves.Reorder(p, oldIndex, newIndex)
        }
        this.setState({drop:null});
    };

    renderHand = (player, card_array, next) => (
        <span style={{marginTop: '20px'}}><Hand value={card_array} hand={true} myG={this.props.G} next={next} mySortStart={this.onSortStart.bind(this)} mySortEnd={this.onSortEnd.bind(this)} myClick={this.onClick.bind(this)} plaincard={this.state.plaincard} /></span>
    );

    renderOtherHand = (player) => {
        let G = this.props.G;
        let ctx = this.props.ctx;
        let currentPlayer = parseInt(ctx.currentPlayer, 10);
        let cardArray = G.remordered[player];
        let hands = cardArray.map(i => <Card value={i} myG={G} plaincard={this.state.plaincard} />);
        let melds = G.meld[player].map(i => this.renderMeld(i));
        let handClass='other';
        if (player === currentPlayer) {
            handClass='curr';
        } else if (G.want[player]) {
            handClass='want';
        } else if (G.autopilot[player]) {
            handClass='auto';
        }
        return <div className='otherhand'><div className={handClass}><span className='vertical'>{this.playerName(player)}</span>{melds} {hands}</div><pre className='chat'>{G.chats[player]}</pre></div>;
    }

    renderMeld = (card_array) => {
        return <span className='meld' style={this.state.plaincard ? {} : {'marginRight': '50px'}}><Hand value={card_array} myClick={this.onClick.bind(this)} myMouseEnter={this.onMouseEnter.bind(this)} myMouseOut={this.onMouseOut.bind(this)} myG={this.props.G} plaincard={this.state.plaincard} /></span>;
    }

    renderPile = (card_array, name) => {
        let ctx = this.props.ctx;
        let current_player = parseInt(ctx.currentPlayer, 10);
        let playing = this.props.playerID != null;
        let this_player = playing ? parseInt(this.props.playerID, 10) : 0;
        let is_current = playing ? current_player === this_player : false;
        let show = false;
        let want = this.props.G.want[this_player];
        if (name === 'discards') {
            show = true;
        }
        if (card_array.length > 0) {
            return <Pile value={card_array} show={show} want={want} myG={this.props.G} myClick={this.onClick.bind(this)} myMouseEnter={this.onMouseEnter.bind(this)} myMouseOut={this.onMouseOut.bind(this)} plaincard={this.state.plaincard} />;
        } else if (is_current) {
            return this.renderButton('disc', 'Discard');
        } else {
            return '';
        }
    }

    renderButton = (id, text) => ( <Button id={id} text={text} myClick={this.onClick.bind(this)} myMouseEnter={this.onMouseEnter.bind(this)} myMouseOut={this.onMouseOut.bind(this)} /> );

    handleRename = (e) => ( this.props.moves.Rename(parseInt(this.props.playerID, 10), e.target.value) );

    handleRechat = (e) => {
        switch (e.target.value) {
        case 'debug':
            this.setState({debug:true});
            break;
        case 'help':
            this.setState({help:true});
	    break;
        default:
            if (this.state.debug) {
                this.setState({debug:false});
            }
        }
        this.props.moves.Rechat(parseInt(this.props.playerID, 10), e.target.value);
    }

    render = () => {
        let G = this.props.G;
        let ctx = this.props.ctx;
        let events = this.props.events;
        let current_player = parseInt(ctx.currentPlayer, 10);
        let playing = this.props.playerID != null;
        let this_player = playing ? parseInt(this.props.playerID, 10) : 0;
        let is_current = playing ? current_player === this_player : false;
        let stage = ctx.activePlayers[current_player];
        let current_name = (G.numPlayers > 1) ? this.playerName(current_player) : 'I';
        let want = this.props.G.want[this_player];

        if (stage === 'wait') {
            stage = 'draw';
            if (is_current) {
                events.setStage('draw');
            }
        }

        if (this.state.psst && ! is_current) {
            this.setState({psst:false});
        } else if (! this.state.psst && is_current) {
            this.setState({psst:true});
            this.playAudio(1);
        }
        if (this.state.pop < G.pop) {
            this.setState({pop:G.pop});
            this.playAudio(0);
        }
        if (this.state.whoosh < G.whoosh) {
            this.setState({whoosh:G.whoosh});
            this.playAudio(2);
        }
        if (this.state.whistle < G.whistle) {
            this.setState({whistle:G.whistle});
            this.playAudio(3);
        }

        let hand = playing ? this.renderHand(this_player, G.players[this_player].hand, G.players[this_player].next) : '';

        let deck = this.renderPile(Array(G.remaining).fill(-9), 'deck');
        if (is_current) {
            if (want) {
                deck = this.renderButton('sell', 'Sell ' + String.fromCodePoint(10233));
            } else if (G.tween) {
                deck = this.renderButton('sell', 'Flip ' + String.fromCodePoint(10233));
            }
        }
        let discards = this.renderPile(G.discards, 'discards');

        let buttons = [this.renderButton('disp', ['Hand', 'Card', 'Text 1', 'Text 2'][this.state.plaincard])];
        if (playing) {
            buttons.push(this.renderButton('cost', 'Cost'));
            buttons.push(this.renderButton('rank', 'Rank'));
            buttons.push(this.renderButton('freq', 'Freq'));
            buttons.push(this.renderButton('suit', 'Suit'));
            buttons.push(this.renderButton('auto', 'Auto'));
            buttons.push(<select value={G.goal[this_player]} onChange={(event) => this.props.moves.SetGoal(this_player, event.target.value)}>{GoalOptions(G, ctx, this_player)}</select>);
            if (is_current) {
                if (stage === 'play' && G.meld[this_player].length === 0) {
                    buttons.push(this.renderButton('meld', 'Meld'));
                } else if (! G.setup) {
                    buttons.push(this.renderButton('deal', 'Start round'));
                    buttons.push(<select value={G.next_round} onChange={(event) => this.props.moves.Jump(event.target.value)}><option value='1'>one</option><option value='2'>two</option><option value='3'>three</option><option value='4'>four</option><option value='5'>five</option><option value='6'>six</option><option value='7'>seven</option></select>);
                    buttons.push(<select value={G.alternate ? 'new' : 'original'} onChange={(event) => this.props.moves.Alternate(event.target.value)}><option value='new'>new 6 round variant</option><option value='original'>original 7 round game</option></select>);
                    buttons.push(<select value={G.wild2 ? 'wild' : 'two'} onChange={(event) => this.props.moves.Wild2(event.target.value)}><option value='wild'>jokers and twos wild</option><option value='two'>only jokers wild</option></select>);
                    buttons.push(<select value={G.final_discard ? 'allow' : 'disallow'} onChange={(event) => this.props.moves.AllowDiscard(event.target.value)}><option value='allow'>allow discard after final meld</option><option value='disallow'>final meld is entire hand</option></select>);
                }
            } else {
                buttons.push(<select value={G.autopilot[this_player]} onChange={(event) => this.props.moves.Autopilot(this_player, event.target.value)}><option value='play'>Play normally</option><option value='skip'>Skip my turn</option><option value='dump'>Dump points</option><option value='auto'>Autopilot</option></select>);
            }
            // buttons.push(this.renderButton('undo', 'Undo'));
        }

        let melds = playing ? G.meld[this_player].map(m => this.renderMeld(m)) : '';

        let handClass='other';

        if (is_current) {
            handClass='curr';
        } else if (G.autopilot[this_player]) {
            handClass='auto';
        }

        let banner = stage;
        switch (stage) {
        case 'deal':
            if (is_current) {
                banner = [this.renderButton('deal', 'Start next round')];
                banner.push(<select value={G.next_round} onChange={(event) => this.props.moves.Jump(event.target.value)}><option value='1'>one</option><option value='2'>two</option><option value='3'>three</option><option value='4'>four</option><option value='5'>five</option><option value='6'>six</option><option value='7'>seven</option></select>);
                banner.push(<select value={G.alternate ? 'new' : 'original'} onChange={(event) => this.props.moves.Alternate(event.target.value)}><option value='new'>new 6 round variant</option><option value='original'>original 7 round game</option></select>);
                banner.push(<select value={G.wild2 ? 'wild' : 'two'} onChange={(event) => this.props.moves.Wild2(event.target.value)}><option value='wild'>jokers and twos wild</option><option value='two'>only jokers wild</option></select>);
                banner.push(<select value={G.final_discard ? 'allow' : 'disallow'} onChange={(event) => this.props.moves.AllowDiscard(event.target.value)}><option value='allow'>allow discard after final meld</option><option value='disallow'>final meld is entire hand</option></select>);
            } else {
                banner = `${current_name} ended the round!`;
            }
            break;
        case 'draw':
            // banner = is_current ? 'DRAW!' : (playing && G.bought[this_player]) ? `WAIT (for ${current_name} to draw)` : playing ? 'BUY?' : 'BUYING';
            if (is_current) {
                if (want) {
                    if (G.tween) {
                        banner = 'SELL or STEAL';
                    } else {
                        banner = 'SELL or DRAW';
                    }
                } else if (G.tween) {
                    banner = 'FLIP or STEAL';
                } else {
                    banner = 'DRAW';
                }
            } else if (playing) {
                if (G.bought[this_player]) {
                    banner = `WAIT for ${current_name} to draw`;
                } else {
                    // banner = '<=== BUY?';
                    banner = String.fromCodePoint(10232) + ' BUY?';
                }
            } else {
                banner = 'BUYING';
            }
            break;
        case 'play':
            if (is_current) {
                if (G.meld[this_player].length > 0) {
                    if (G.melded) {
                        banner = 'TOSS or DISCARD';
                    } else {
                        banner = 'DISCARD';
                    }
                } else {
                    banner = 'MELD or DISCARD';
                }
            } else if (playing) {
                if (G.bought[this_player] || G.bought[current_player]) {
                    banner = `WAIT for ${current_name} to discard`;
                } else if (G.discards.length > 0) {
                    banner = String.fromCodePoint(10232) + ' STEAL?';
                } else {
                    banner = String.fromCodePoint(10232) + ` WAIT for ${current_name} to discard`;
                }
            } else {
                banner = `WAITING for ${current_name} to discard`;
            }
            break;
        case 'discard':
            banner = is_current ? 'DISCARD' : playing ? `WAIT for ${current_name} to discard` : `WAITING for ${current_name} to discard`;
            break;
        case 'wait':
            break;
        default:
            alert('oops ' + stage);
        }
        if (G.round + (G.alternate ? 1 : 0) > 7) {
            banner = 'GAME OVER!!!';
        }

        let goal = `round ${G.round}/${G.alternate ? 6 : 7}`;
        if (7 === G.round + G.alternate ? 1 : 0) {
            goal += G.final_discard ? " (meld with optional discard)" : " (must meld entire hand)"
        }
        if (G.wild2) {
            goal += '; twos wild';
        }
        if (G.round + (G.alternate ? 1 : 0) > 7) {
            goal = 'game over';
        } else if (stage === 'deal') {
            goal = 'round over';
        }

        let other_hands = playing ? [] : [this.renderOtherHand(0)];
        for (let offset = 1; offset < G.numPlayers; offset++) {
            let player = (this_player + offset) % G.numPlayers;
            other_hands.push(this.renderOtherHand(player));
        }

        let scores = <React.Fragment>
            <tr id='header'>{ [...Array(G.numPlayers).keys()].map((i) => (<th colspan="2">{this.playerName(i)}</th>)) }</tr>
            {
                // one row per round so far
		G.scores.slice(0, G.round - 1).map((scores, r) => ( <tr>{ scores.map((playerScore, p) => ( <><td class="score">{playerScore}</td><td class="meld">{G.meldstrings[r][p]}</td></> )) }</tr> ))
            }
            {
                // footer row for sum of 2 or more rounds
                (G.round > 2) && <tr id='footer'>
                    {
                        G.scores
                            // add each row of scores to the running total list
                            .reduce((previous, scores) => (
                                previous.map((previousPlayerScore, index) => (previousPlayerScore + scores[index]))
                            ), Array(G.numPlayers).fill(0))
                            // display each element of that list
                            .map((playerScore) => ( <><th class="score">{playerScore}</th><th></th></> ))
                    }
                </tr>
            }
        </React.Fragment>

        let name = playing ? <input type='text' id='name' name='name' value={this.playerName(this_player)} required minlength='1' maxlength='30' size='30' onChange={this.handleRename.bind(this)} /> : 'Spectator';
        let chat = playing ? <div className='chat'>status:<input type='text' id='chat' chat='chat' value={G.chats[this_player]} required minlength='1' maxlength='250' size='50' onChange={this.handleRechat.bind(this)} /></div> : '';

        return (
            <div className='board'>
                <Helmet>
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+KR:wght@700&family=Noto+Sans+JP:wght@700&family=Noto+Sans+Symbols+2&display=swap" rel="stylesheet" />
                </Helmet>
                <audio className='audio-element'><source src={pop}></source></audio>
                <audio className='audio-element'><source src={psst}></source></audio>
                <audio className='audio-element'><source src={whoosh}></source></audio>
                <audio className='audio-element'><source src={whistle}></source></audio>
                <pre id='log' className={classNames({
                         messy: playing && G.players[this_player].mess,
                })}>{(this.state.debug ? (JSON.stringify(this.state) + '\n') : '') + G.log.concat(playing ? G.players[this_player].mess : '').join('\n')}</pre>
                <div id='scores'><table>
                <tbody>{scores}</tbody>
                </table></div>
                <div>{deck} {discards}<span className='banner'>{banner}</span></div>
                <div className={handClass}>{hand}{chat}</div>
                <div className='mymeld'>{melds}</div>
                <div className='buttons'><p>{buttons} {goal}; name:{name}</p></div>
                <div>{other_hands}</div>
            </div>
        );
    }
}
