import { PlayerView } from 'boardgame.io/core';
import arrayMove from 'array-move';

const nCard = (i) => ( i < 0 ? -1 : i % 52 );
const nRank = (i) => ( i < 0 ? -1 : (i + 12) % 13 );
const nSuit = (i) => ( i < 0 ? -1 : Math.floor(i % 52 / 13) );
const nCdrA = (i) => ( i < 0 ? -1 : 13 * nSuit(i) + nRank(i));
const isWild = (G, ctx, i) => ( i < 0 || (G.wild2 && i % 13 === 1) );
const Cost = (G, ctx, i) => ( isWild(G, ctx, i) ? 25 : i % 13 === 0 ? 15 : i % 13 >= 7 ? 10 : 5 );

const ShortCardText = (i) => (
    i < 0 ? '%' + String.fromCodePoint(9734) : ['2','3','4','5','6','7','8','9','10','J','Q','K','A'][nRank(i)] + String.fromCodePoint([9827, 9830, 9824, 9829][nSuit(i)])
);

const CardText = (i) => (
    i < 0 ? 'joker' : ['two','three','four','five','six','seven','eight','nine','ten','jack','queen','king','ace'][nRank(i)] + ' of ' + ['clubs', 'diamonds', 'spades', 'hearts'][nSuit(i)]
);

const isSet = (G, ctx, a) => {
    // must have at least 3 cards
    if (a.length < 3) {
        return '';
    }

    // okay to have 3 cards of which no more than 1 is a joker and all non-jokers are twos
    if (a.length === 3 && a.filter(x => x < 0).length <= 1 && a.filter(x => x > 0 && x % 13 !== 1).length === 0) {
        return 'twos';
    }

    // at least two cards must not be wild
    if (a.filter(x => ! isWild(G, ctx, x)).length < 2) {
        return '';
    }

    let b = -1;
    for (let i = 0; i < a.length; i++) {
        if (! isWild(G, ctx, a[i])) {
            if (isWild(G, ctx, b)) {
                b = a[i] % 13;
            } else if (! isWild(G, ctx, b) && b !== a[i] % 13) {
                return '';
            }
        }
    }

    return ['aces', 'twos', 'threes', 'fours', 'fives', 'sixes', 'sevens', 'eights', 'nines', 'tens', 'jacks', 'queens', 'kings'][b];
}

const isRun = (G, ctx, a) => {
    // must have at least 4 cards
    if (a.length < 4) {
        return '';
    }

    // establish base of putative run
    let b = a[0] % 52;
    if (isWild(G, ctx, a[0])) {
        if (! isWild(G, ctx, a[1])) {
            b = a[1] % 52 - 1;
        } else if (! isWild(G, ctx, a[2])) {
            b = a[2] % 52 - 2;
        } else if (!isWild(G, ctx, a[3])) {
            b = a[3] % 52 - 3;
        } else {
            // shouldn't happen (four jokers) but whatever
            return 'jokers';
        }
    }

    let run = ['clubs', 'diamonds', 'spades', 'hearts'];

    for (let i = 0; i < a.length; i++) {
        if (! isWild(G, ctx, a[i]) && a[i] % 52 !== b + i) {
            // ending on a high ace
            if (i === a.length - 1 && a[i] % 13 === 0 && a[i] % 52 + 13 === b + i) {
                let goodCount = 0;
                for (let j = 0; j < a.length - 1; j++) {
                    if (a[j] >= 0 && a[j] % 52 === b + j) {
                        goodCount++;
                    }
                }
                if (goodCount > 1 || (goodCount === 1 && a[a.length - 1] >= 0 && a[a.length - 1] % 52 + 13 === b + a.length - 1)) {
                    return run[nSuit(b)];
                } else {
                    return '';
                }
            } else {
                return '';
            }
        } else if (! isWild(G, ctx, a[i]) && nSuit(a[i]) !== nSuit(b)) {
            return '';
        }
    }

    let goodCount = 0;
    for (let j = 0; j < a.length; j++) {
        if (a[j] >= 0 && a[j] % 52 === b + j) {
            goodCount++;
        }
    }
    if (goodCount > 1) {
        return run[nSuit(b)];
    } else {
        return '';
    }
}

const EndStage = (G, ctx, events) => {
    Score(G, ctx);
    G.round++;
    G.whistle++;

    let n = G.numPlayers;
    for (let p = 0; p < n; p++) {
        G.remembered[p] = G.players[p].hand;
        G.remordered[p] = G.players[p].hand;
    }

    events.setActivePlayers({
        currentPlayer: 'deal',
        others: 'wait',
    });
}

const Jump = ({G, ctx}, round) => {
    let r = parseInt(round, 10);
    if (r >= 1 && r + (G.alternate ? 1 : 0) <= 7) {
        G.next_round = r;
    }
}

const Wild2 = ({G, ctx}, w2) => {
    G.wild2 = w2 === 'wild';
}

const Alternate = ({G, ctx}, a) => {
    G.alternate = a === 'new';
    G.goals = G.alternate ? ['sr','rr','ssr','srr','rrr','ssrr*'] : ['ss','sr','rr','sss','ssr','srr','rrr*']
    Jump(G, ctx, '1');
}

const AllowDiscard = ({G, ctx}, a) => {
    G.final_discard = a === 'allow';
}

const doDeal = ({G, ctx, events, random}) => {
    G.round = G.next_round;
    let n = G.numPlayers;
    G.numDecks = n < 5 ? 2 : n < 10 ? 3 : 4;
    G.remaining = G.numDecks * 54;
    G.secret.deck = Array(G.remaining);
    for (let i = 0; i < G.numDecks * 54; i++) {
        G.secret.deck[i] = i - 2 * G.numDecks;
    }
    G.secret.deck = random.Shuffle(G.secret.deck); // comment this line and possibly
    // G.secret.deck.reverse(); // uncomment this line for easy debugging scenario
    G.discards = [G.secret.deck.pop()];
    let handSize = G.alternate ? 8 : 6 + G.round;
    const arr = [...Array(8).keys()].map(i => i + 1)
    for (let j = 0; j < n; j++) {
        // in the original game give everyone the same goal, dictated by the round
        // in the alternate game give everyone the first goal unmelded by the player
        const set = new Set(G.melds[j])
        G.goal[j] = (G.alternate && G.round !== 6) ? arr.find(g => !set.has(g)) : G.round;
        G.want[j] = false;
        G.bought[j] = false;
        G.players[j].hand = [];
        G.players[j].next = null;
        G.meld[j] = [];
        G.remembered[j] = [];
        G.remordered[j] = [];
        for (let i = 0; i < handSize; i++) {
            G.players[j].hand.push(G.secret.deck.pop());
            G.remordered[j].push(null);
        }
    }
    G.remaining -= n * (handSize) + 1;
    let nextPlayer = ((G.round - 1) % n).toString(10);
    for (let i = 0; i < n; i++) {
        let np = (i + G.round - 1) % n;
        if (! G.autopilot[np]) {
            nextPlayer = np.toString(10);
            break;
        } else {
            Shamble(G, ctx, random, np);
        }
    }
    let sets = G.alternate ? [1,0,2,1,0,2][(G.round-1) % 6] : [2,1,0,3,2,1,0][(G.round-1) % 7];
    let runs = G.alternate ? [1,2,1,2,3,2][(G.round-1) % 6] : [0,1,2,0,1,2,3][(G.round-1) % 7];
    sets = `${sets} set${sets !== 1 ? 's' : ''}`;
    runs = `${runs} run${runs !== 1 ? 's' : ''}`;
    let goal = `started round ${G.round}: ${sets}, ${runs}`;
    G.log = [ goal ];
    G.tween = false;

    return nextPlayer;
}
const Deal = ({G, ctx, events, random}) => {
    let nextPlayer = doDeal({G, ctx, random});
    events.endTurn({ next: nextPlayer });
}

const Draw = ({G, ctx, events, random}, fromDeck) => {
    let c = parseInt(ctx.currentPlayer, 10);
    doDraw(G, ctx, events, random, c, fromDeck);
}

const doDraw = (G, ctx, events, random, c, fromDeck) => {
    let n = G.numPlayers;
    let hand = G.players[c].hand;
    G.players[c].mess = '';

    G.setup = true;

    if (G.remaining <= 2) {
        let new_deck = G.discards;
        let top_discard = new_deck.pop();
        G.discards = [top_discard];
        G.secret.deck = random.Shuffle(new_deck).concat(G.secret.deck[0]);
        G.remaining = G.secret.deck.length;
    }

    if (fromDeck) {
        for (let i = 1; i < n; i++) {
            let p = (c + i) % n;
            if (G.want[p]) {
                hand = G.players[p].hand;
                let discard = G.discards.pop();
                G.remembered[p].push(discard);
                hand.push(discard);
                G.remordered[p].push(discard);
                G.players[p].next = hand[hand.length - 1];
                hand.push(G.secret.deck.pop());
                G.remordered[p].push(null);
                G.remaining--;
                if (G.remaining === 0) {
                    // TODO shuffle discards
                }
                if (G.discards.length === 0) {
                    G.discards.push(G.secret.deck.pop());
                    G.remaining--;
                }
                G.want = Array(n).fill(false);
                if (n > 2) {
                    G.bought[p] = true;
                }
                G.log.push(G.names[p] + ' bought the ' + CardText(discard));
                if (G.tween) {
                    G.tween = false;
                    let extra = G.secret.deck.pop();
                    G.discards.push(extra);
                    G.log.push('extra discard ' + CardText(extra) + ' added from deck');
                    G.remaining--;
                }
                return;
            }
        }
        if (! G.tween) {
            hand.push(G.secret.deck.pop());
            G.remordered[c].push(null);
            G.remaining--;
            G.want = Array(n).fill(false);
            G.log.push(G.names[c] + ' drew from the deck');
            events.endStage();
        }
    } else {
        if (n > 2) {
            G.bought[c] = true; // abuse of "bought" but useful for allowing STEAL functionality
        }
        let discard = G.discards.pop();
        G.remembered[c].push(discard);
        hand.push(discard);
        G.remordered[c].push(discard);
        if (G.tween) {
            G.log.push(G.names[c] + ' stole the ' + CardText(discard));
            G.players[c].next = hand[hand.length - 1];
            hand.push(G.secret.deck.pop());
            G.remordered[c].push(null);
            G.remaining--;
        } else {
            G.log.push(G.names[c] + ' grabbed the ' + CardText(discard));
            events.endStage();
        }
        G.want = Array(n).fill(false);
    }
    if (G.tween) {
        G.tween = false;
        let extra = G.secret.deck.pop();
        G.discards.push(extra);
        G.log.push('extra discard ' + CardText(extra) + ' added from deck');
        G.remaining--;
    } else {
        G.players[c].next = hand[hand.length - 1];
    }

    G.melded = G.meld[c].length > 0;
}

const Steal = ({G, ctx}, p, id) => {
    G.players[p].mess = '';
    G.autopilot[p] = '';
    let c = parseInt(ctx.currentPlayer, 10);
    if (! G.bought[c] && ! G.bought[p]) {
        // this player hasn't bought a card since the last discard
        if (id === G.discards[G.discards.length - 1]) {
            G.discards.pop();
            let hand = G.players[p].hand;
            G.remembered[p].push(id);
            hand.push(id);
            G.remordered[p].push(id);
            G.players[p].next = hand[hand.length - 1];
            hand.push(G.secret.deck.pop());
            G.remordered[p].push(null);
            G.remaining--;
            if (G.remaining === 0) {
                // TODO shuffle discards
            }
        }
        if (G.numPlayers > 2) {
            G.bought[p] = true;
        }
        G.log.push(G.names[p] + ' stole the ' + CardText(id));
    }
}

const Autopilot = ({G, ctx}, p, b) => {
    G.autopilot[p] = b === 'play' ? '' : b;
}

const Evaluate = (code, base) => {
    let score = 0;
    let parts = getAutoParts(base);
    let sets = parts.sets;
    let runs = parts.runs;
    let best = {score:0, hands:[]};
    best = FastMeld(code, sets, runs, 0, [], best);
    if (best.score > 0) {
        return 100 * best.score;
    }
    if (code.includes('s')) {
        score += sets[0].length ? 2 : sets[1].length ? 1 : 0;
    }
    if (code.includes('r')) {
        score += runs[0].length ? 4 : runs[1].length ? 2 : runs[2].length ? 1 : 0;
    }
    // console.log(String(score) + ' ' + base.map(ShortCardText).join(''));
    return score;
}

const Shamble = (G, ctx, random, c) => {
    if (G.autopilot[c] === 'dump') {
        let hand = G.players[c].hand;
        let discard_cost = Cost(G, ctx, G.discards[G.discards.length - 1])
        if (discard_cost === 5) {
            let discard = G.discards.pop();
            G.remembered[c].push(discard);
            hand.push(discard);
            G.remordered[c].push(discard);
            G.log.push(G.names[c] + ' grabbed the ' + CardText(discard));
        } else {
            hand.push(G.secret.deck.pop());
            G.remordered[c].push(null);
            G.remaining--;
            G.log.push(G.names[c] + ' drew from the deck');
        }
        Sort({G, ctx}, c, 'cost');
        G.players[c].next = hand[hand.length - 1];
        doDiscard(G, ctx, c);
    } else if (G.autopilot[c] === 'auto') {
        let hand = G.players[c].hand;
        let top_discard = G.discards[G.discards.length - 1];
        let discard_wild = isWild(G, ctx, top_discard)
        let code = GoalCode(G, ctx, c);
        let base = hand.filter(id => !isWild(G, ctx, id));
        let old_score = Evaluate(code, base);
        let new_score = Evaluate(code, base.concat([top_discard]));
        if (discard_wild || new_score > old_score) {
            let discard = G.discards.pop();
            G.remembered[c].push(discard);
            hand.push(discard);
            G.remordered[c].push(discard);
            G.log.push(G.names[c] + ' grabbed the ' + CardText(discard));
        } else {
            hand.push(G.secret.deck.pop());
            G.remordered[c].push(null);
            G.remaining--;
            G.log.push(G.names[c] + ' drew from the deck');
        }
        if (G.meld[c].length > 0) {
            for (let x of hand) {
                G.players[c].next = x;
                doToss(G, ctx, c, null)
            }
            Sort({G, ctx}, c, 'cost');
            G.players[c].next = hand[hand.length - 1];
        } else {
            Sort({G, ctx}, c, (code[0] === 's') ? 'freq' : 'suit');
            doAutoMeld(G, ctx, random, c);
            Meld({G, ctx}, c);
        }
        hand = G.players[c].hand;
        // console.log(hand.map(x=>ShortCardText(x)).join(''));
        if (hand.length > 0 && G.round + (G.alternate ? 1 : 0) <= 7) {
            G.players[c].next = hand[hand.length - 1];
            doDiscard(G, ctx, c);
        }
    }
}

const Rename = ({G, ctx}, p, name) => {
    G.names[p] = name;
}

const Rechat = ({G, ctx, events}, p, chat) => {
    G.autopilot[p] = '';
    G.chats[p] = chat;
    let ppp = parseInt(p, 10);
    if (chat === 'zombify') {
        for (let pp = 0; pp < G.numPlayers; pp++) {
            if (ppp === pp) {
                G.chats[pp] = 'zzzzz';
            } else {
                G.autopilot[pp] = 'skip';
            }
        }
    } else if (chat === 'autobots') {
        for (let pp = 0; pp < G.numPlayers; pp++) {
            if (ppp === pp) {
                G.chats[pp] = 'roll out';
            } else {
                G.autopilot[pp] = 'auto';
            }
        }
    } else if (chat === 'the interrupting cow say') {
        G.chats[p] = 'MOOOOOO!';
        events.endTurn({ next: p });
    }
}

const Reorder = ({G, ctx}, p, oldIndex, newIndex) => {
    G.players[p].mess = '';
    G.autopilot[p] = '';
    let hand = G.players[p].hand;

    arrayMove.mutate(hand, oldIndex, newIndex);
    G.remordered[p] = hand.map(i => G.remembered[p].includes(i) ? i : null);
}

const SetGoal = ({G, ctx}, p, b) => {
    G.goal[p] = parseInt(b, 10);
}

const Sort = ({G, ctx}, p, rsf) => {
    G.players[p].mess = '';
    if (G.autopilot[p] === 'skip') {
        G.autopilot[p] = '';
    }
    let hand = G.players[p].hand;

    if (rsf === 'rank') {
        hand.sort((a, b) => 52 * (nRank(a) - nRank(b)) + nCard(a) - nCard(b));
    } else if (rsf === 'suit') {
        hand.sort((a, b) => 13 * (nSuit(a) - nSuit(b)) + nRank(a) - nRank(b));
    } else if (rsf === 'cost') {
        hand.sort((a, b) => 100000 * (Cost(G, ctx, a) - Cost(G, ctx, b)) + 52 * (nRank(a) - nRank(b)) + nCard(a) - nCard(b));
    } else {
        let freq = Array(13).fill(0);
        for (let i = 0; i < hand.length; i++) {
            if (hand[i] >= 0) {
                freq[nRank(hand[i])]++;
            }
        }
        hand.sort((a, b) => (b < 0 ? 1000000 : 1000 * freq[nRank(b)] - nRank(b)) - (a < 0 ? 1000000 : 1000 * freq[nRank(a)] - nRank(a)));
    }
    G.remordered[p] = hand.map(i => G.remembered[p].includes(i) ? i : null);
}

const Buy = ({G, ctx}, p) => {
    G.players[p].mess = '';
    G.autopilot[p] = '';
    // toggle wanting to buy the top discard
    if (! G.bought[p]) {
        // this player hasn't bought a card since the last discard
        G.want[p] = ! G.want[p];
        let c = parseInt(ctx.currentPlayer, 10);
        G.want[c] = G.want.slice(0, c).includes(true) || G.want.slice(c + 1).includes(true);
    }
}

const Ready = ({G, ctx}, p, id) => {
    G.autopilot[p] = '';
    // toggle designation of card to discard or toss
    if (G.players[p].next === id) {
        G.players[p].next = null;
    }
    else {
        G.players[p].next = id;
    }
}

const ReadyIndex = ({G, ctx}, p, index) => {
    G.players[p].next = G.players[p].hand[index];
}

const CheckSet = (G, ctx, a) => (
    isSet(G, ctx, a) ? '' : a.map(i => ShortCardText(i)).join('-') + ' is not a set'
);

const CheckRun = (G, ctx, a) => (
    isRun(G, ctx, a) ? '' : a.map(i => ShortCardText(i)).join('-') + ' is not a run'
);

const CheckNotKissing = (G, ctx, a, b) => (
    (isRun(G, ctx, a.concat(b)) || isRun(G, ctx, b.concat(a))) ? a.map(i => ShortCardText(i)).join('-') + ' and ' + b.map(i => ShortCardText(i)).join('-') + ' are kissing runs' : ''
);

const GoalCode = (G, ctx, p) => (
    G.goals[G.goal[p]-1]
)

const CheckMeld = (G, ctx, p) => {
    let failure = '';

    let hand = G.players[p].hand;
    let code = GoalCode(G, ctx, p);
    let d = G.final_discard ? 1 : 0;

    if (code === 'ss') {
        return CheckSet(G, ctx, hand.slice(0, 3)) || CheckSet(G, ctx, hand.slice(3, 6));
    } else if (code === 'sr') {
        return CheckSet(G, ctx, hand.slice(0, 3)) || CheckRun(G, ctx, hand.slice(3, 7));
    } else if (code === 'rr') {
        let a = hand.slice(0, 4);
        let b = hand.slice(4, 8);
        return CheckRun(G, ctx, a) || CheckRun(G, ctx, b) || CheckNotKissing(G, ctx, a, b);
    } else if (code === 'sss') {
        return CheckSet(G, ctx, hand.slice(0, 3)) || CheckSet(G, ctx, hand.slice(3, 6)) || CheckSet(G, ctx, hand.slice(6, 9));
    } else if (code === 'ssr') {
        return CheckSet(G, ctx, hand.slice(0, 3)) || CheckSet(G, ctx, hand.slice(3, 6)) || CheckRun(G, ctx, hand.slice(6, 10));
    } else if (code === 'srr') {
        let a = hand.slice(0, 3);
        let b = hand.slice(3, 7);
        let c = hand.slice(7, 11);
        return CheckSet(G, ctx, a) || CheckRun(G, ctx, b) || CheckRun(G, ctx, c) || CheckNotKissing(G, ctx, b, c);
    } else if (code === 'rrr') { // penultimate round of alternate variant
        let a = hand.slice(0, 4);
        let b = hand.slice(4, 8);
        let c = hand.slice(8, 12);
        return CheckRun(G, ctx, a) || CheckRun(G, ctx, b) || CheckRun(G, ctx, c) || CheckNotKissing(G, ctx, a, b) || CheckNotKissing(G, ctx, b, c) || CheckNotKissing(G, ctx, a, c);
    } else if (code === 'rrr*') { // ultimate round of original game
        for (let i = 4; i <= hand.length - 8; i++) {
            if (isRun(G, ctx, hand.slice(0, i))) {
                for (let j = i + 4; j <= hand.length - 4; j++) {
                    if (isRun(G, ctx, hand.slice(i, j))) {
                        for (let k = hand.length; k >= hand.length - d; k--) {

                            if (isRun(G, ctx, hand.slice(j, k))) {
                                // check for kissing runs
                                let runs = [hand.slice(0, i), hand.slice(i, j), hand.slice(j, k)];
                                for (let left = 0; left < 2; left++) {
                                    for (let right = left + 1; right < 3; right++) {
                                        failure = failure || CheckNotKissing(G, ctx, runs[left], runs[right]);
                                    }
                                }
                                return failure;
                            }

                        }
                    }
                }
            }
        }
    } else if (code === 'ssrr*') { // ultimate round of alternate variant
        for (let i = 3; i <= hand.length - 11; i++) {
            if (isSet(G, ctx, hand.slice(0, i))) {
                for (let ii = i + 3; ii <= hand.length - 8; ii++) {
                    if (isSet(G, ctx, hand.slice(i, ii))) {
                        for (let j = ii + 4; j <= hand.length - 4; j++) {
                            if (isRun(G, ctx, hand.slice(ii, j))) {
                                for (let k = hand.length; k >= hand.length - d; k--) {
                                    if (isRun(G, ctx, hand.slice(j, k))) {
                                        // check for kissing runs
                                        return CheckNotKissing(G, ctx, hand.slice(ii, j), hand.slice(j, k))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return failure || 'no meld found';
}

const choose = (a, n) => {
  const result = [];
  const sub_choose = (aa, start) => {
    if (aa.length === n) {
      result.push(aa);
      return;
    }
    for (let i = start; i < a.length; i++) {
      sub_choose([...aa, a[i]], i+1);
    }
  }
  for (let i = 0; i < a.length; i++) {
    sub_choose([a[i]], i + 1);
  }
  return result;
}

const groupBy = (a, f) => {
    let r = {};
    for (let e of a) {
        let key = f(e);
        if (key in r) {
            r[key].push(e);
        }
        else {
            r[key] = [e];
        }
    }
    return r
}

const overlap = (a, b) => (a.some(i => b.includes(i)));

const AutoMeld = ({G, ctx, random}, p) => {
    doAutoMeld(G, ctx, random, p);
}
const doAutoMeld = (G, ctx, random, p) => {
    let hand = G.players[p].hand;
    let code = GoalCode(G, ctx, p);
    let need = code.split('').reduce((a,c)=>{return a+{s:3,r:4,'*':0}[c]},0);
    let realneed = code.split('').reduce((a,c)=>{return a+{s:2,r:2,'*':0}[c]},0);
    let wild = hand.filter(id => isWild(G, ctx, id));
    let base = hand.filter(id => !isWild(G, ctx, id));
    let mess = "no meld found";
    if (need > hand.length) {
        mess = "This round requires at least " + (need - hand.length) + " more cards to meld.";
    } else if (realneed > base.length) {
        mess = "This round requires at least " + (realneed - base.length) + " more non-wild cards to meld.";
    } else if (G.meld[p].length > 0) {
        mess = "already melded";
    } else {
        let parts = getAutoParts(base);
        let best = {score:0, hands:[]};
        best = FastMeld(code, parts.sets, parts.runs, 0, [], best);
        hand = best.hands.length > 0 ? random.Shuffle(best.hands)[0] : [];

        // at this point we have tried all combinations of sets and runs, and have one of the ones that use the fewest number of wildcards
        // or perhaps []
        if (hand.length > 0) {
            let left = base.filter(i => !hand.includes(i)).concat(wild);
            for (let i = 0; i < hand.length; i++) {
                if (hand[i] === WILD) {
                    hand[i] = left.pop();
                }
            }
            left.reverse();
            hand = hand.concat(left);
            G.players[p].hand = hand;
            G.remordered[p] = hand.map(i => G.remembered[p].includes(i) ? i : null);
            mess = CheckMeld(G, ctx, p) || "found a meld!";
        } else {
            mess = "not even close to a meld";
        }
    }
    G.players[p].mess = mess;
}
const getAutoParts = (base) => {
    let sets = {0:[],1:[]};

    let rank = groupBy(base, nRank);

    for (let r in rank) {
        if (rank[r].length >= 3) {
            sets[0] = sets[0].concat(choose(rank[r], 3));
            sets[1] = sets[1].concat(choose(rank[r], 2));
        } else if (rank[r].length === 2) {
            sets[1].push(rank[r]);
        }
    }

    const getRuns = (card) => {
        let runs = {0:[],1:[],2:[]};
        let keys = Object.keys(card).map(i=>parseInt(i,10)).sort((a,b)=>(a-b));
        for (let i = 1; i < keys.length; i++) {
            // 2 cards within 3
            if (nSuit(keys[i]) === nSuit(keys[i-1]) && keys[i] <= keys[i-1]+3) {
                for (let c1 of card[keys[i-1]]) {
                    for (let c2 of card[keys[i]]) {
                        // make 1 run with 2 wilds
                        runs[2].push([c1, c2]);
                    }
                }
            }
            // 3 cards within 3
            if (i > 1 && nSuit(keys[i]) === nSuit(keys[i-2]) && keys[i] <= keys[i-2]+3) {
                for (let c1 of card[keys[i-2]]) {
                    for (let c2 of card[keys[i-1]]) {
                        for (let c3 of card[keys[i]]) {
                            // make 1 run with 1 wild
                            runs[1].push([c1, c2, c3]);
                            // make 1 runs with 2 wilds
                            runs[2].push([c1, c3]);
                        }
                    }
                }
            }
            // 4 cards within 3
            if (i > 2 && nSuit(keys[i]) === nSuit(keys[i-3]) && keys[i] <= keys[i-3]+3) {
                for (let c1 of card[keys[i-3]]) {
                    for (let c2 of card[keys[i-2]]) {
                        for (let c3 of card[keys[i-1]]) {
                            for (let c4 of card[keys[i]]) {
                                // make 1 run with 0 wilds
                                runs[0].push([c1, c2, c3, c4]);
                                // make 2 runs with 1 wild each
                                runs[1].push([c1, c2, c4]);
                                runs[1].push([c1, c3, c4]);
                            }
                        }
                    }
                }
            }
        }
        return runs;
    }
    let runs = getRuns(groupBy(base, nCdrA));
    let alow = getRuns(groupBy(base, nCard));
    for (let i of [0, 1, 2]) {
        runs[i] = runs[i].concat(alow[i].filter(x => nRank(x[0]) === 12));
    }

    return {sets:sets,runs:runs};
}

const WILD = -99;
const FastMeld = (code, sets, runs, depth, sofar, best) => {
    const bank = Array(2).fill(WILD);
    const real = a => a.filter(x => x !== WILD).length;

    if (depth === code.length || code[depth] === '*') {
        let newscore = real(sofar);
        if (best.length === 0) {
            // first time
            best = {score:newscore, hands: [sofar]};
        } else {
            let oldscore = best.score;
            if (newscore > oldscore) {
                best = {score: newscore, hands: [sofar]};
            } else if (newscore === oldscore) {
                best.hands.push(sofar);
            }
        }
        return best;
    } else if (code[depth] === 's') {
        for (let w = 0; w <= 1; w++) {
            for (let set of sets[w]) {
                if (! overlap(sofar, set)) {
                    let add = set.slice().concat(bank.slice(0, w));
                    best = FastMeld(code, sets, runs, depth + 1, sofar.slice().concat(add), best);
                }
            }
        }
    } else if (code[depth] === 'r') {
        for (let w = 0; w <= 2; w++) {
            for (let run of runs[w]) {
                if (! overlap(sofar, run)) {
                    let add = run.slice();
                    let r1, r2, r3, w1, w2;
                    switch (w) {
                    case 0:
                        add = bank.slice(0, w).concat(add);
                        break;
                    case 1:
                        [r1, r2, r3, w1] = add.concat(bank.slice(0, 1));
                        if (nRank(r3) - nRank(r1) === 2 || nRank(r1) - nRank(r3) === 11) {
                            add = (nRank(r2) > 7) ? [w1, r1, r2, r3] : [r1, r2, r3, w1];
                        } else if (nRank(r2) - nRank(r1) === 2 || nRank(r1) - nRank(r2) === 11) {
                            add = [r1, w1, r2, r3];
                        } else {
                            add = [r1, r2, w1, r3];
                        }
                        break;
                    case 2:
                        [r1, r2, w1, w2] = add.concat(bank.slice(0,2));
                        switch (nRank(r2) - nRank(r1)) {
                        case 1:
                        case -12:
                            add = (nRank(r2) > 7) ? [w1, w2, r1, r2] : [r1, r2, w1, w2];
                            break;
                        case 2:
                        case -11:
                            add = (nRank(r2) > 7) ? [w1, r1, w2, r2] : [r1, w1, r2, w2];
                            break;
                        case 3:
                        case -10:
                            add = [r1, w1, w2, r2];
                            break;
                        default:
                            console.log("oops");
                        }
                        break;
                    default:
                        console.log("oops");
                    }
                    best = FastMeld(code, sets, runs, depth + 1, sofar.slice().concat(add), best);
                }
            }
        }
    }
    return best;
}

const Meld = ({G, ctx, events}, p) => {
    G.players[p].mess = '';
    let hand = G.players[p].hand;
    let code = GoalCode(G, ctx, p);
    let need = code.split('').reduce((a,c)=>{return a+{s:3,r:4,'*':0}[c]},0);
    if (need > hand.length) {
        G.players[p].mess = "This round requires at least " + (need - hand.length) + " more cards to meld.";
        return;
    } else if (G.meld[p].length > 0) {
        G.players[p].mess = 'already melded';
        return;
    }

    let d = G.final_discard ? 1 : 0;

    G.players[p].mess = CheckMeld(G, ctx, p);
    if (G.players[p].mess) {
        return;
    }

    G.whoosh++;

    G.melds[p].push(G.goal[p]);
    G.meldstrings[G.round - 1][p] = code;

    if (code === 'ss') {
        G.meld[p] = [hand.slice(0, 3), hand.slice(3, 6)];
        G.log.push(G.names[p] + ' melded with ' + isSet(G, ctx, G.meld[p][0]) + ' and ' + isSet(G, ctx, G.meld[p][1]));
        hand.splice(0, 6);
        G.remordered[p].splice(0, 6);
    } else if (code === 'sr') {
        G.meld[p] = [hand.slice(0, 3), hand.slice(3, 7)];
        G.log.push(G.names[p] + ' melded with ' + isSet(G, ctx, G.meld[p][0]) + ' and ' + isRun(G, ctx, G.meld[p][1]));
        hand.splice(0, 7);
        G.remordered[p].splice(0, 7);
    } else if (code === 'rr') {
        G.meld[p] = [hand.slice(0, 4), hand.slice(4, 8)];
        G.log.push(G.names[p] + ' melded with ' + isRun(G, ctx, G.meld[p][0]) + ' and ' + isRun(G, ctx, G.meld[p][1]));
        hand.splice(0, 8);
        G.remordered[p].splice(0, 8);
    } else if (code === 'sss') {
        G.meld[p] = [hand.slice(0, 3), hand.slice(3, 6), hand.slice(6, 9)];
        G.log.push(G.names[p] + ' melded with ' + isSet(G, ctx, G.meld[p][0]) + ', ' + isSet(G, ctx, G.meld[p][1]) + ', and ' + isSet(G, ctx, G.meld[p][2]));
        hand.splice(0, 9);
        G.remordered[p].splice(0, 9);
    } else if (code === 'ssr') {
        G.meld[p] = [hand.slice(0, 3), hand.slice(3, 6), hand.slice(6, 10)];
        G.log.push(G.names[p] + ' melded with ' + isSet(G, ctx, G.meld[p][0]) + ', ' + isSet(G, ctx, G.meld[p][1]) + ', and ' + isRun(G, ctx, G.meld[p][2]));
        hand.splice(0, 10);
        G.remordered[p].splice(0, 10);
    } else if (code === 'srr') {
        G.meld[p] = [hand.slice(0, 3), hand.slice(3, 7), hand.slice(7, 11)];
        G.log.push(G.names[p] + ' melded with ' + isSet(G, ctx, G.meld[p][0]) + ', ' + isRun(G, ctx, G.meld[p][1]) + ', and ' + isRun(G, ctx, G.meld[p][2]));
        hand.splice(0, 11);
        G.remordered[p].splice(0, 11);
    } else if (code === 'rrr') {
        G.meld[p] = [hand.slice(0, 4), hand.slice(4, 8), hand.slice(8, 12)];
        G.log.push(G.names[p] + ' melded with ' + isRun(G, ctx, G.meld[p][0]) + ', ' + isRun(G, ctx, G.meld[p][1]) + ', and ' + isRun(G, ctx, G.meld[p][2]));
        hand.splice(0, 12);
        G.remordered[p].splice(0, 12);
    } else if (code === 'rrr*') {
        // eslint-disable-next-line
        find_meld: {
            for (let i = 4; i <= hand.length - 8; i++) {
                let run1 = isRun(G, ctx, hand.slice(0, i));
                if (run1) {
                    for (let j = i + 4; j <= hand.length - 4; j++) {
                        let run2 = isRun(G, ctx, hand.slice(i, j));
                        if (run2) {
                            for (let k = hand.length; k >= hand.length - d; k--) {
                                let run3 = isRun(G, ctx, hand.slice(j, k));
                                if (run3) {
                                    G.log.push(G.names[p] + ' melded with ' + run1 + ', ' + run2 + ', and ' + run3);
                                    if (k < hand.length) {
                                        let discard = hand.pop();
                                        G.discards.push(discard);
                                        G.log.push(G.names[p] + ' discarded the ' + CardText(discard));
                                    }
                                    G.remembered[p] = G.players[p].hand.splice(0);
                                    G.remordered[p] = G.remembered[p];
                                    Score(G, ctx);
                                    for (let pp = 0; pp < G.numPlayers; pp++) {
                                        if (p !== pp) {
                                            G.remembered[pp] = G.players[pp].hand;
                                            G.remordered[pp] = G.players[pp].hand;
                                        }
                                    }
                                    G.players[p].hand = G.remembered[p].slice();
                                    G.round = 8; // just so person who melded can see scores
                                    // eslint-disable-next-line
                                    break find_meld;
                                }
                            }
                        }
                    }
                }
            }
        }
    } else if (code === 'ssrr*') {
        // eslint-disable-next-line
        find_meld: {
            for (let i = 3; i <= hand.length - 11; i++) {
                let set1 = isSet(G, ctx, hand.slice(0, i));
                if (set1) {
                    for (let ii = i+3; ii <= hand.length - 8; ii++) {
                        let set2 = isSet(G, ctx, hand.slice(i, ii));
                        if (set2) {
                            for (let j = ii + 4; j <= hand.length - 4; j++) {
                                let run1 = isRun(G, ctx, hand.slice(ii, j));
                                if (run1) {
                                    for (let k = hand.length; k >= hand.length - d; k--) {
                                        let run2 = isRun(G, ctx, hand.slice(j, k));
                                        if (run2) {
                                            G.log.push(G.names[p] + ' melded with ' + set1 + ', ' + set2 + ', ' + run1 + ', and ' + run2);
                                            if (k < hand.length) {
                                                let discard = hand.pop();
                                                G.discards.push(discard);
                                                G.log.push(G.names[p] + ' discarded the ' + CardText(discard));
                                            }
                                            G.remembered[p] = G.players[p].hand.splice(0);
                                            G.remordered[p] = G.remembered[p];
                                            Score(G, ctx);
                                            for (let pp = 0; pp < G.numPlayers; pp++) {
                                                if (p !== pp) {
                                                    G.remembered[pp] = G.players[pp].hand;
                                                    G.remordered[pp] = G.players[pp].hand;
                                                }
                                            }
                                            G.players[p].hand = G.remembered[p].slice();
                                            G.round = 7; // just so person who melded can see scores
                                            // eslint-disable-next-line
                                            break find_meld;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    let meld = G.meld[p];
    for (let i = 0; i < meld.length; i++) {
        for (let j = 0; j < meld[i].length; j++) {
            let card = meld[i][j];
            let ind = G.remembered[p].indexOf(card);
            if (ind >= 0) {
                G.remembered[p].splice(ind, 1);
            }
        }
    }
    G.players[p].next = null;

    if (hand.length === 0 && code[code.length-1] !== '*') {
        EndStage(G, ctx, events);
    }
}

const Toss = ({G, ctx, events}, p, id) => {
    let c = parseInt(ctx.currentPlayer, 10);
    let card = G.players[p].next;

    if (p !== c || card == null || ! G.melded) {
        return;
    }

    let success = doToss(G, ctx, p, id)

    if (success) {
        let hand = G.players[p].hand;
        if (hand.length === 0) {
            EndStage(G, ctx, events);
        }
    }
}

const doToss = (G, ctx, p, id) => {
    G.players[p].mess = '';
    let n = G.numPlayers;
    let hand = G.players[p].hand;
    let card = G.players[p].next;

    let success = false;

    // eslint-disable-next-line
    find_toss: {
        for (let i = 0; i < n; i++) {
            let meld = G.meld[i];
            for (let j = 0; j < meld.length; j++) {
                if (id === null || meld[j].includes(id)) {
                    let set = isSet(G, ctx, meld[j]);
                    let run = isRun(G, ctx, meld[j]);
                    let code = GoalCode(G, ctx, p);

                    // try to trade for a wild if round isn't all sets and card isn't wild
                    if (code !== 'ss' && code !== 'sss' && !isWild(G, ctx, card)) {
                        for (let k = 0; k < meld[j].length; k++) {
                            if (isWild(G, ctx, meld[j][k])) {
                                let may = meld[j].slice(0, k).concat(card, meld[j].slice(k + 1));
                                if ((set && isSet(G, ctx, may)) || (run && isRun(G, ctx, may))) {
                                    let ind = hand.indexOf(card);
                                    let wildcard = meld[j][k];
                                    G.remembered[p].push(wildcard);
                                    G.remordered[p].push(wildcard);
                                    hand[ind] = wildcard;
                                    G.players[p].next = wildcard;
                                    G.meld[i][j][k] = card;
                                    G.log.push(G.names[p] + ' used the ' + CardText(card) + ' to get the ' + CardText(wildcard));
                                    if (id === null) {
                                        doToss(G, ctx, p, null);
                                    }
                                    success = true;
                                    // eslint-disable-next-line
                                    break find_toss;
                                }
                            }
                        }
                    }

                    // try to add card to beginning or end of meld, prioritizing end closer to where the user clicked
                    let may = (meld[j].indexOf(id) > meld[j].length / 2) ? [meld[j].concat([card]), [card].concat(meld[j])] : [[card].concat(meld[j]), meld[j].concat([card])];
                    for (let k = 0; k < may.length; k++) {
                        if ((set && isSet(G, ctx, may[k])) || (run && isRun(G, ctx, may[k]))) {
                            G.meld[i][j] = may[k];
                            let ind = hand.indexOf(card);
                            hand.splice(ind, 1);
                            G.log.push(G.names[p] + ' tossed the ' + CardText(card));
                            success = true;
                            // eslint-disable-next-line
                            break find_toss;
                        }
                    }
                }
            }
        }
    }

    if (success) {
        G.players[p].next = null;
        G.pop++;

        let ind = G.remembered[p].indexOf(card);
        if (ind >= 0) {
            G.remembered[p].splice(ind, 1);
        }

        ind = hand.indexOf(card);
        G.remordered[p].splice(ind, 1);
    }
    return success;
}

const doDiscard = (G, ctx, p) => {
    G.players[p].mess = '';
    let hand = G.players[p].hand;

    let card = G.players[p].next;
    if (card != null) {
        let ind = hand.indexOf(card);
        hand.splice(ind, 1);
        G.players[p].next = null;
    } else {
        return;
    }

    let ind = G.remembered[p].indexOf(card);
    if (ind >= 0) {
        G.remembered[p].splice(ind, 1);
    }

    ind = hand.indexOf(card);
    G.remordered[p].splice(ind, 1);

    G.discards.push(card);
    if (! G.autopilot[p]) {
        G.log = [];
    }
    G.log.push([G.names[p] + ' discarded the ' + CardText(card)]);
}

const Discard = ({G, ctx, events, random}, p) => {
    let hand = G.players[p].hand;
    G.players[p].mess = '';
    let n = G.numPlayers;

    G.want = Array(n).fill(false);
    G.bought = Array(n).fill(false);

    if (! p) {
        p = parseInt(ctx.currentPlayer, 10);
    }

    doDiscard(G, ctx, p);

    // players on autopilot want to have enough cards to meld
    let code = GoalCode(G, ctx, p);
    let need = code.split('').reduce((a,c)=>{return a+{s:3,r:4,'*':0}[c]},0);
    let realneed = code.split('').reduce((a,c)=>{return a+{s:2,r:2,'*':0}[c]},0);
    for (let i = 0; i < n; i++) {
        let otherhand = G.players[i].hand;
        let otherbase = otherhand.filter(id => !isWild(G, ctx, id));
        if (G.autopilot[i] === 'auto') {
            G.want[i] = false;
            if (G.meld[i].length === 0) {
                let top_discard = G.discards[G.discards.length - 1];
                let discard_wild = isWild(G, ctx, top_discard)
                if (discard_wild || otherbase.length < realneed || otherhand.length < need) {
                    G.want[i] = true;
                }
            }
            if (n <= 2) {
                G.tween = true;
                doDraw(G, ctx, events, random, i, !G.want[i]);
            }
        }
    }

    if (n <= 2) {
        // two player variant: discard the top card of the deck after chance to buy other player's discard
        G.tween = true;
    }
    if (hand.length === 0) {
        G.want = Array(n).fill(false);
        EndStage(G, ctx, events);
    } else if (! G.autopilot[p]) {
        let turnover = false;
        let nextPlayer = ((p + 1) % n).toString(10);
        for (let i = 0; i < n; i++) {
            let np = (i + p + 1) % n;
            if (! G.autopilot[np]) {
                nextPlayer = np.toString(10);
                break;
            } else {
                if (! turnover) {
                    Shamble(G, ctx, random, np);
                    if (G.players[np].hand.length === 0) {
                        turnover = true;
                    }
                }
            }
        }
        if (turnover) {
            EndStage(G, ctx, events);
        } else {
            G.want[nextPlayer] = G.want.slice(0, nextPlayer).includes(true) || G.want.slice(nextPlayer + 1).includes(true);
            events.endTurn({ next: nextPlayer });
        }
    }
    // recompute as a workaround for bug
    G.remordered[p] = hand.map(i => G.remembered[p].includes(i) ? i : null);
}

const Score = (G, ctx) => {
    let n = G.numPlayers;
    for (let i = 0; i < n; i++) {
        let hand = G.players[i].hand;
        G.scores[G.round - 1][i] = 0;
        for (let j = 0; j < hand.length; j++) {
            let card = hand[j];
            G.scores[G.round - 1][i] += Cost(G, ctx, card);
        }
    }
    G.next_round = G.round + 1;
}

const moveMap = {
    Alternate: Alternate,
    Autopilot: Autopilot,
    Buy: Buy,
    Deal: Deal,
    Discard: Discard,
    Draw: Draw,
    Jump: Jump,
    Meld: Meld,
    AutoMeld: AutoMeld,
    Ready: Ready,
    ReadyIndex: ReadyIndex,
    Rechat: Rechat,
    Rename: Rename,
    Reorder: Reorder,
    SetGoal: SetGoal,
    Sort: Sort,
    Steal: Steal,
    Toss: Toss,
    Wild2: Wild2,
    AllowDiscard: AllowDiscard,
};

const makeMoves = (moves, undoable, client) => {
    let o = {};
    moves.forEach((i) => { o[i] = { move: moveMap[i], undoable: undoable, client: client } });
    return o;
}

const clientMoves = makeMoves(['Rename', 'Rechat', 'Reorder', 'SetGoal', 'Sort', 'AutoMeld', 'Ready', 'ReadyIndex'], true, true);

export const rummy = {
    name: 'rummy',
    minPlayers: 1,
    maxPlayers: 50,
    setup: ({ctx, events, random}, setupdata) => {
        let N = ctx.numPlayers;
        let n = N > 1 ? N : 2;
        let G = {
            numPlayers: n,
            secret: { deck: [] },
            discards: [],
            round: 1,
            next_round: 1,
            scores: Array(8).fill(Array(n).fill(0)),
            meldstrings: Array(8).fill(Array(n).fill('')),
            melds: Array(n).fill([]),
            best: Array(n).fill(0),
            want: Array(n).fill(false),
            goal: Array(n).fill(1),
            goals: ['sr','rr','ssr','srr','rrr','ssrr*'],
            bought: Array(n).fill(false),
            autopilot: Array(n).fill(''),
            remembered: Array(n).fill([]),
            remordered: Array(n).fill([]),
            remaining: 0,
            players: {},
            names: Array(n).fill(''),
            chats: Array(n).fill(''),
            meld: Array(n).fill([]),
            melded: false,
            alternate: true,
            wild2: true,
            final_discard: false,
            log: [],
            setup: false,
            tween: false,
            pop: 0,
            whoosh: 0,
            whistle: 0,
        };
        if (N <= 2) {
            G.autopilot[1] = 'auto';
        }
        for (let i = 0; i < n; i++) {
            G.players[i] = {};
            G.players[i].hand = [];
            G.players[i].next = null;
            G.players[i].mess = '';
        }
        doDeal({G, ctx, random});

        return G;
    },

    playerView: PlayerView.STRIP_SECRETS,

    turn: {
        activePlayers: {
            currentPlayer: 'draw',
            others: 'wait',
        },

        onBegin: ({G, ctx, events}) => {
            events.setActivePlayers({
                currentPlayer: 'draw',
                others: 'wait',
            });
        },
        stages: {
            deal: {
                moves: Object.assign(makeMoves(['Deal', 'Alternate', 'Jump', 'Wild2', 'AllowDiscard'], true, false), clientMoves),
            },
            wait: {
                moves: Object.assign(Object.assign(makeMoves(['Buy', 'Steal'], true, false), makeMoves(['Autopilot'], true, true)), clientMoves),
            },
            draw: {
                moves: Object.assign(makeMoves(['Deal', 'Alternate', 'Jump', 'Wild2', 'AllowDiscard', 'Draw'], true, false), clientMoves),
                next: 'play',
            },
            play: {
                moves: Object.assign(makeMoves(['Meld', 'Toss', 'Discard'], true, false), clientMoves),
                next: 'discard',
            },
            discard: {
                moves: Object.assign(makeMoves(['Discard'], true, false), clientMoves),
            },
        },
    },
}
