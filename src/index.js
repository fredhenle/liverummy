import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import { rummy } from "./game";
import { rummyboard } from "./board";

import { Lobby } from 'boardgame.io/react';
const server = `https://${window.location.hostname}`;
const importedGames = [{ game: rummy, board: rummyboard }];

ReactDOM.render(
  <React.StrictMode>
    <title>LiveRummy</title>
    <div id='lobby' style={{margin: '10px'}}><p>Welcome to LiveRummy!  See <a target='_blank' rel='noopener noreferrer' href='https://docs.google.com/document/d/1LjjORJ2Xc7KK4hUyaRRCh_B7JKurMRviW05V9otOkrA/edit?usp=sharing'>rules</a>.</p><p>Report issues to <a target='_blank' rel='noopener noreferrer' href='mailto:fredhenle@gmail.com'>fredhenle@gmail.com</a>.</p></div>
    <Lobby gameServer={server} lobbyServer={server} gameComponents={importedGames} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
